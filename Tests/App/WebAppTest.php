<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Unit Test
 * @package   facebook-utils
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2013 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://github.com/chrisnoden
 */

namespace ChrisNoden\Tests\Comms;

use ChrisNoden\Facebook\App\WebApp;
use ChrisNoden\Facebook\Graph\Object\Application;

class WebAppTest extends \PHPUnit_Framework_TestCase
{

    public function testBasicObject()
    {
        if (defined('TEST_APP_ID') && defined('TEST_APP_SECRET')) {
            $app = Application::createFromGraph(TEST_APP_ID, TEST_APP_SECRET);
            $obj = new WebApp($app);
            $this->assertInstanceOf('ChrisNoden\Facebook\App\WebApp', $obj);
            $this->assertEquals($app, $obj->getApplication());
        } else {
            $this->fail(
                'Please create a test_settings.php file with constants for TEST_APP_ID and TEST_APP_SECRET'
            );
        }
    }


    /**
     * @depends testBasicObject
     */
    public function testLoginUrlBasicFuntionality()
    {
        if (defined('TEST_APP_ID') && defined('TEST_APP_SECRET')) {
            $app = Application::createFromGraph(TEST_APP_ID, TEST_APP_SECRET);
            $obj = new WebApp($app);

            $loginUrl = $obj->getLoginUrl();
            $parts = parse_url($loginUrl);
            $this->assertEquals('https', $parts['scheme']);
            $this->assertEquals('www.facebook.com', $parts['host']);
            $this->assertEquals('/dialog/oauth', $parts['path']);
            parse_str($parts['query'], $qs);
            $this->assertEquals(TEST_APP_ID, $qs['client_id']);
            $this->assertEquals('', $qs['scope']);
        } else {
            $this->fail(
                'Please create a test_settings.php file with constants for TEST_APP_ID and TEST_APP_SECRET'
            );
        }
    }
}
