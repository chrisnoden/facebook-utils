<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Unit Test
 * @package   facebook-utils
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2013 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://github.com/chrisnoden
 */

namespace ChrisNoden\Tests\Comms;

use ChrisNoden\Facebook\App\SignedRequest;
use ChrisNoden\Facebook\Graph\Object\Application;

class SignedRequestTest extends \PHPUnit_Framework_TestCase
{

    public function testEncodeDecode()
    {
        if (defined('TEST_APP_ID') && defined('TEST_APP_SECRET') && defined('TEST_FACEBOOK_USER_ID')) {
            $app = Application::createFromGraph(TEST_APP_ID, TEST_APP_SECRET);
            $obj = new SignedRequest($app);
            $this->assertInstanceOf('ChrisNoden\Facebook\App\SignedRequest', $obj);

            $code = md5('make a sensible length string');
            $issued_at = time();
            $obj->setCode($code);
            $obj->setAlgorithm('HMAC-SHA256');
            $obj->setIssuedAt($issued_at);
            $obj->setUserId(TEST_FACEBOOK_USER_ID);

            $dec = new SignedRequest($app);
            $dec->setSignedRequest($obj->createSignedRequest());

            $this->assertEquals($code, $dec->getCode());
            $this->assertEquals('HMAC-SHA256', $dec->getAlgorithm());
            $this->assertEquals($issued_at, $dec->getIssuedAt());
            $this->assertEquals(TEST_FACEBOOK_USER_ID, $dec->getUserId());
        } else {
            $this->fail(
                'Please create a test_settings.php file with constants for TEST_APP_ID and TEST_APP_SECRET'
            );
        }
    }

}
