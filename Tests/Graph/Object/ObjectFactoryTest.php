<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Unit Test
 * @package   facebook-graph
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2013 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://github.com/chrisnoden
 */

namespace ChrisNoden\Tests\Graph\Object;

use ChrisNoden\Facebook\Graph\AccessToken\AccessTokenType;
use ChrisNoden\Facebook\Graph\GraphObjectType;
use ChrisNoden\Facebook\Graph\Object\ObjectAbstract;
use ChrisNoden\Facebook\Graph\Object\ObjectFactory;

class ObjectFactoryTest extends \PHPUnit_Framework_TestCase
{

    protected $facebook_extended_permissions = array(
        'user_about_me',
        'user_activities',
        'user_birthday',
        'user_checkins',
        'user_education_history',
        'user_events',
        'user_groups',
        'user_hometown',
        'user_interests',
        'user_likes',
        'user_location',
        'user_notes',
        'user_online_presence',
        'user_photo_video_tags',
        'user_photos',
        'user_relationships',
        'user_relationship_details',
        'user_religion_politics',
        'user_status',
        'user_videos',
        'user_website',
        'user_work_history',
        'email',
        'read_friendlists',
        'read_insights',
        'read_mailbox',
        'read_requests',
        'read_stream',
        'xmpp_login',
        'ads_management',
        'create_event',
        'manage_friendlists',
        'manage_notifications',
        'offline_access',
        'publish_checkins',
        'publish_stream',
        'rsvp_event',
        'sms',
        'publish_actions',
        'manage_pages'
    );


    /**
     * Iterate round all the GraphObjectType supported Object names
     * and instantiate a new Object using the Factory::create method
     */
    public function testObjectCreation()
    {
        foreach (GraphObjectType::members() as $graph_object) {
            /** @var GraphObjectType $graph_object */
            $this->assertInstanceOf('ChrisNoden\Facebook\Graph\GraphObjectType', $graph_object);
            $class_name = strtoupper($graph_object->value());
            $obj = ObjectFactory::create(GraphObjectType::$class_name());
            $this->assertInstanceOf('ChrisNoden\Facebook\Graph\Object\ObjectAbstract', $obj);
            $this->assertInstanceOf('ChrisNoden\Facebook\Graph\Object\\'.$class_name, $obj);
            $this->assertEquals($graph_object->value(), $obj->__toString());

            $this->checkGraphObject($obj);

            // dispose of the object before next iteration
            $obj = null;
            unset($obj);
        }
    }


    protected function checkGraphObject(ObjectAbstract $obj)
    {
        // all objects have an ID field - if not then this will throw an InvalidArgumentException
        $obj->getFieldDetails('id');

        $fields = $obj->getFieldList();
        foreach ($fields as $param_name => $properties) {
            if (!isset($properties['description'])) {
                $this->fail(
                    sprintf(
                        'Object %s field %s missing description property',
                        $obj->__toString(),
                        $param_name
                    )
                );
            }
            if (!isset($properties['permissions'])) {
                $this->fail(
                    sprintf(
                        'Object %s field %s missing permissions property',
                        $obj->__toString(),
                        $param_name
                    )
                );
            }
            $this->checkPermissionsValue($obj->__toString(), $param_name, $properties['permissions']);

            if (!isset($properties['returns'])) {
                $this->fail(
                    sprintf(
                        'Object %s field %s missing returns property',
                        $obj->__toString(),
                        $param_name
                    )
                );
            }
            $this->checkReturnsValue($obj->__toString(), $param_name, $properties['returns']);

            if (!isset($properties['editable'])) {
                $this->fail(
                    sprintf(
                        'Object %s field %s missing editable property',
                        $obj->__toString(),
                        $param_name
                    )
                );
            }
            if (!isset($properties['must_ask'])) {
                $this->fail(
                    sprintf(
                        'Object %s field %s missing must_ask property',
                        $obj->__toString(),
                        $param_name
                    )
                );
            }
        }
    }


    /**
     * Tests the permissions of each object property to ensure it's valid
     *
     * @param $object_name
     * @param $field_name
     * @param $value
     */
    protected function checkPermissionsValue($object_name, $field_name, $value)
    {
        if ($value === false) {
            return;
        }

        /** @var AccessTokenType $val */
        foreach (AccessTokenType::members() as $key => $val) {
            if ($val->value() === $value) {
                // passed
                return;
            }
        }

        // test if this is an extended permission
        $arr = explode(',', $value);
        foreach ($arr as $test_value) {
            if (in_array($test_value, $this->facebook_extended_permissions)) {
                // passed
                return;
            }
        }

        $this->fail(
            sprintf(
                'Object \'%s\' field \'%s\' has invalid permissions property \'%s\'',
                $object_name,
                $field_name,
                $value
            )
        );
    }


    /**
     * Test the 'returns' value of the field is a valid type
     *
     * @param $object_name
     * @param $field_name
     * @param $value
     */
    protected function checkReturnsValue($object_name, $field_name, $value)
    {
        $test = 1;

        if (!@settype($test, $value)) {
            $this->fail(
                sprintf(
                    'Object \'%s\' field \'%s\' has invalid returns property \'%s\'',
                    $object_name,
                    $field_name,
                    $value
                )
            );
        }
    }


    /**
     * A valid Facebook object but currently not supported by this library
     */
    public function testUnsupportedObjectType()
    {
        $this->setExpectedException('PHPUnit_Framework_Error');
        $obj = ObjectFactory::create('review');
    }
}
