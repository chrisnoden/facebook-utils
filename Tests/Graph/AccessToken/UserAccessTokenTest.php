<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Unit Test
 * @package   facebook-utils
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2013 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://github.com/chrisnoden
 */

namespace ChrisNoden\Tests\Graph\AccessToken;

use ChrisNoden\Facebook\Graph\AccessToken\UserAccessToken;
use ChrisNoden\Facebook\Graph\AccessToken\AppAccessToken;

class UserAccessTokenTest extends \PHPUnit_Framework_TestCase
{

    private $test_code = 'AQBZTpL6J57TxFyBPMjh8CrdlKa95gjnAFlY80KwnZcsh2uSOnSM8WT_3TqYpNkO6lOONTX6hQkJ86U1dfiBDAfpzdYvqeM_NvhiCSUmmPbXIvCJNot5e__-2dqE9tcbTE07ZyODuyYcSo_jnfT_wjkqy9GIahLarHj1PYdz9TXaOCmbgrKlyq07mtUw-iyBCI6qyj_g5GsuzdA5dR08fahWII3bgNMKZ1Yyw6QDiGeKrS8aAJZoh9YVwi3PDd_B7nWHVJWylgEPUEazGCcLuzE3fdontNrf5vWDIGq9n7rN2cBd6G-tr4GJli59N6xLnP0';

    private $test_redirect_url = 'https://apps.facebook.com/gambino_dev';


    public function testBasicObject()
    {
        if (defined('TEST_APP_ID') && defined('TEST_APP_SECRET')) {
            $obj = UserAccessToken::create(TEST_APP_ID, TEST_APP_SECRET, $this->test_code, $this->test_redirect_url);
            $this->assertInstanceOf('ChrisNoden\Facebook\Graph\AccessToken\UserAccessToken', $obj);
            $this->assertInstanceOf('ChrisNoden\Facebook\Graph\AccessToken\AccessTokenAbstract', $obj);
        } else {
            $this->fail(
                'Please create a test_settings.php file with two constants for TEST_APP_ID and TEST_APP_SECRET'
            );
        }
    }


    public function testTokenInfo()
    {
        if (defined('TEST_APP_ID') && defined('TEST_APP_SECRET') && defined('TEST_USER_ACCESS_TOKEN')) {
            $obj = UserAccessToken::create(TEST_APP_ID, TEST_APP_SECRET, $this->test_code, $this->test_redirect_url);
            $obj->setAccessToken(TEST_USER_ACCESS_TOKEN);

            // get the info about our token
            $data = $obj->getTokenInfo();

            $this->assertEquals(TEST_APP_ID, $data['app_id']);
            $this->assertTrue($data['is_valid']);
            $this->assertEquals($obj->isValid(), $data['is_valid']);
            $this->assertEquals(TEST_APP_NAME, $data['application']);
            $this->assertEquals(explode(',', TEST_APP_SCOPES), $data['scopes']);
            $this->assertEquals(TEST_FACEBOOK_USER_ID, $data['user_id']);
            $this->assertArrayHasKey('issued_at', $data);
            $this->assertArrayHasKey('expires_at', $data);
        }
    }

}
