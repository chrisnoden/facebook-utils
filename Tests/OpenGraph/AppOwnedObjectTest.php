<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Unit Test
 * @package   facebook-utils
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2013 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://github.com/chrisnoden
 */

namespace ChrisNoden\Tests\OpenGraph;

use ChrisNoden\Facebook\Graph\AccessToken\AppAccessToken;
use ChrisNoden\Facebook\OpenGraph\AppOwnedObject;

class AppOwnedObjectTest extends \PHPUnit_Framework_TestCase
{

    public function testCreateObject()
    {
        if (defined('TEST_OG_APP_NAMESPACE') && defined('TEST_OG_OBJECT_TYPE')) {
            $obj = new AppOwnedObject();
            $obj->setAccessToken(AppAccessToken::create(TEST_APP_ID, TEST_APP_SECRET));
            $obj->setAppNamespace(TEST_OG_APP_NAMESPACE);
            $obj->setObjectType(TEST_OG_OBJECT_TYPE);
            $obj->setDescription('Sample Object - Test Case');
            $obj->setTitle('Sample Object');
            $obj->setImageUrl('https://s-static.ak.fbcdn.net/images/devsite/attachment_blank.png');
            $obj->setUrl('https://dev.pkgamo.co.uk/gambino');
            $obj->create();
            var_dump($obj);
        }
    }

}
