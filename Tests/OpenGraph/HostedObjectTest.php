<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Unit Test
 * @package   facebook-utils
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2013 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://github.com/chrisnoden
 */

namespace ChrisNoden\Tests\OpenGraph;

use ChrisNoden\Facebook\Graph\Object\Application;
use ChrisNoden\Facebook\OpenGraph\HostedObject;

class HostedObjectTest extends \PHPUnit_Framework_TestCase
{

    public function testHostedObjectContent()
    {
        if (defined('TEST_APP_ID') && defined('TEST_APP_SECRET')) {
            $application = Application::createFromGraph(TEST_APP_ID, TEST_APP_SECRET);
            $application->load(TEST_APP_ID, array('app_domains','website_url'));
            $obj = new HostedObject();
            $obj->setApplication($application);
            $obj->setTitle('Test Title');
            $obj->setUrl($application->getWebsiteUrl());
            $obj->setImageUrl($application->getLogoUrl());
            var_dump($obj->getContent());
        }
    }
}
