<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Unit Test
 * @package   facebook-graph
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2013 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://github.com/chrisnoden
 */

namespace ChrisNoden\Tests\Comms;

use ChrisNoden\Facebook\Comms\AppNotification;
use ChrisNoden\Facebook\Graph\Object\Application;
use ChrisNoden\Facebook\Graph\Api\GraphRequest;

class AppNotificationTest extends \PHPUnit_Framework_TestCase
{



    public function testObjectInstantiation()
    {
        $obj = new AppNotification();
        $this->assertInstanceOf('ChrisNoden\Facebook\Comms\AppNotification', $obj);
    }


    /**
     * Test all the main App Notification request is correctly constructed
     */
    public function testValidNotificationRequest()
    {
        if (defined('TEST_APP_ID') && defined('TEST_APP_SECRET') && defined('TEST_FACEBOOK_USER_ID')) {
            $application = new Application();
            $application->setId(TEST_APP_ID);
            $application->setSecret(TEST_APP_SECRET);
            $access_token = $application->getAccessToken();

            $notification = AppNotification::create()
                ->setAccessToken($access_token)
                ->setMessage('This is a test message')
                ->setFacebookUserId(TEST_FACEBOOK_USER_ID)
                ->setRef('test ref');
            /** @var GraphRequest $request */
            $request = $notification->getRequest();
            $data = parse_url($request->getRequest()->getUrl());

            $this->assertEquals('https', $data['scheme']);
            $this->assertEquals('graph.facebook.com', $data['host']);
            $query = $data['query'];
            parse_str($query, $item);
            $this->assertEquals('This is a test message', $item['template']);
            $this->assertEquals('test_ref', $item['ref']);
            $this->assertEquals($access_token->getAccessToken(), $item['access_token']);
            $this->assertEquals(3, count($item));
        } else {
            $this->fail(
                'Please create a test_settings.php file with constants for TEST_APP_ID and TEST_APP_SECRET and TEST_FACEBOOK_USER_ID'
            );
        }
    }


    /**
     * Test the href parameters are passed correctly
     *
     * @depends testValidNotificationRequest
     */
    public function testNotificationParameters()
    {
        $application = new Application();
        $application->setId(TEST_APP_ID);
        $application->setSecret(TEST_APP_SECRET);
        $access_token = $application->getAccessToken();

        $notification = AppNotification::create()
            ->setAccessToken($access_token)
            ->setMessage('This is a test message')
            ->setFacebookUserId(TEST_FACEBOOK_USER_ID)
            ->setParameters(
                array(
                    'chris'  => 'noden',
                    'testID' => 12345654321
                )
            );
        /** @var GraphRequest $request */
        $request = $notification->getRequest();
        $data = parse_url($request->getRequest()->getUrl());
        $query = $data['query'];
        parse_str($query, $item);
        $this->assertEquals('?chris=noden&amp;testID=12345654321', $item['href']);
    }


    public function testSendToInvalidUserId()
    {
        if (defined('TEST_APP_ID') && defined('TEST_APP_SECRET')) {
            $application = new Application();
            $application->setId(TEST_APP_ID);
            $application->setSecret(TEST_APP_SECRET);
            $access_token = $application->getAccessToken();

            $notification = AppNotification::create()
                ->setAccessToken($access_token)
                ->setMessage('This is a test message')
                ->setFacebookUserId('123456');
            $notification->isValid();
            $this->setExpectedException('ChrisNoden\Facebook\Exception\FacebookInsufficientPermissions');
            $notification->send();
        }
    }


}
