<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   facebook-utils
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2013 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://github.com/chrisnoden
 */

namespace ChrisNoden\Facebook\Core;

use Eloquent\Enumeration\AbstractEnumeration;

/**
 * Class FacebookDomainMapping
 *
 * @category ChrisNoden\Facebook
 * @package  facebook-utils
 * @author   Chris Noden <chris.noden@gmail.com>
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link     https://github.com/chrisnoden/synergy
 */
final class FacebookDomainMapping extends AbstractEnumeration
{

    const API         = 'https://api.facebook.com/';
    const API_VIDEO   = 'https://api-video.facebook.com/';
    const API_READ    = 'https://api-read.facebook.com/';
    const GRAPH       = 'https://graph.facebook.com/';
    const GRAPH_VIDEO = 'https://graph-video.facebook.com/';
    const WWW         = 'https://www.facebook.com/';
}
