<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   facebook-utils
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2013 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://github.com/chrisnoden
 */

namespace ChrisNoden\Facebook\OpenGraph;

use ChrisNoden\Facebook\Core\FacebookDomainMapping;
use ChrisNoden\Facebook\Graph\AccessToken\AppAccessToken;
use Guzzle\Http\Client;

/**
 * Class AppOwnedObject
 * The Facebook App Owned Object as a manageable entity
 *
 * @category ChrisNoden\Facebook\OpenGraph
 * @package  facebook-utils
 * @author   Chris Noden <chris.noden@gmail.com>
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link     https://github.com/chrisnoden/synergy
 * @link     https://developers.facebook.com/docs/opengraph/using-object-api/#creatingapp
 */
class AppOwnedObject extends ObjectAbstract implements OwnedObjectInterface
{


    /**
     * Build the HTTP request to create our object
     *
     * @return \Guzzle\Http\Message\RequestInterface
     */
    public function getGraphCreateRequest()
    {
        $client = new Client(FacebookDomainMapping::GRAPH()->value());
        $client->setConfig(array('exceptions' => true));
        $request = $client->post(
            sprintf('/app/objects/%s.%s', $this->app_namespace, $this->object_type),
            array(),
            array(
                'access_token' => $this->access_token->getAccessToken(),
                'object'       => json_encode($this->getObject(), JSON_UNESCAPED_SLASHES)
            ),
            array(
                'exceptions'      => true,
                'connect_timeout' => $this->connection_timeout,
            )
        );

        echo "Access Token: ".$this->access_token->getAccessToken(). "/n";
        var_dump(json_encode($this->getObject(), JSON_UNESCAPED_SLASHES));
        $query = $request->getQuery();
        $query->useUrlEncoding(false);

        return $request;
    }


    /**
     * @param AppAccessToken $access_token
     */
    public function setAccessToken(AppAccessToken $access_token)
    {
        $this->access_token = $access_token;
    }
}
