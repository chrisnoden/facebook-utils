<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   facebook-utils
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2013 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://github.com/chrisnoden
 */

namespace ChrisNoden\Facebook\OpenGraph;

use ChrisNoden\Facebook\Core\FacebookDomainMapping;
use ChrisNoden\Facebook\Graph\AccessToken\AccessTokenAbstract;
use ChrisNoden\Facebook\Graph\Object\User;
use Guzzle\Http\Client;

/**
 * Class Story
 *
 * @category ChrisNoden\Facebook\OpenGraph
 * @package  facebook-utils
 * @author   Chris Noden <chris.noden@gmail.com>
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link     https://github.com/chrisnoden/synergy
 */
class Story
{

    /** @var User */
    protected $user;
    /** @var string */
    protected $access_token;
    /** @var string */
    protected $object_name;
    /** @var string */
    protected $object_value;
    /** @var string */
    protected $story;


    public static function create()
    {
        return new Story();
    }


    /**
     * Post the story to Open Graph
     * ignores any errors
     *
     * @return void
     */
    public function post()
    {
        $client = new Client(FacebookDomainMapping::GRAPH()->value());
        $client->setConfig(array('exceptions' => true));
        $request = $client->post(
            sprintf('/%s/%s', $this->user->getId(), $this->story),
            array(),
            array(
                'access_token'     => $this->access_token,
                'method'           => 'POST',
                $this->object_name => $this->object_value
            ),
            array(
                'exceptions'      => true,
                'connect_timeout' => 10,
            )
        );

        try {
            $request->send();
        } catch (\Exception $ex) {
            // ignore any failure and move on
        }
    }


    /**
     * Set the value of access_token member
     *
     * @param string $access_token
     *
     * @return $this
     */
    public function setAccessToken($access_token)
    {
        if ($access_token instanceof AccessTokenAbstract) {
            $this->access_token = $access_token->getAccessToken();
        } else {
            $this->access_token = (string)$access_token;
        }

        return $this;
    }


    /**
     * Set the value of object_name member
     *
     * @param string $object_name
     *
     * @return $this
     */
    public function setObjectName($object_name)
    {
        $this->object_name = $object_name;

        return $this;
    }


    /**
     * Set the value of object_value member
     *
     * @param string $object_value
     *
     * @return $this
     */
    public function setObjectValue($object_value)
    {
        $this->object_value = $object_value;

        return $this;
    }


    /**
     * Set the value of user member
     *
     * @param \ChrisNoden\Facebook\Graph\Object\User $user
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }


    /**
     * Set the story type (eg "myapp:play")
     *
     * @param string $story
     *
     * @return $this
     */
    public function setStory($story)
    {
        $this->story = $story;

        return $this;
    }
}
