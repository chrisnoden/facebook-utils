<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   facebook-utils
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2013 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://github.com/chrisnoden
 */

namespace ChrisNoden\Facebook\OpenGraph;

/**
 * Class HostedObject
 * Create the HTML elements for a Self Hosted Object for Open Graph
 *
 * <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# object: http://ogp.me/ns/object#">
 * <meta property="fb:app_id" content="your app ID" />
 * <meta property="og:type"   content="object" />
 * <meta property="og:url"    content="Put your own URL to the object here" />
 * <meta property="og:title"  content="Sample Object" />
 * <meta property="og:image"  content="https://s-static.ak.fbcdn.net/images/devsite/attachment_blank.png" />
 *
 * @category ChrisNoden\Facebook\OpenGraph
 * @package  facebook-utils
 * @author   Chris Noden <chris.noden@gmail.com>
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link     https://github.com/chrisnoden/synergy
 */
class HostedObject extends ObjectAbstract
{

    public function getContent()
    {
        $head = sprintf(
            "<head prefix=\"og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# object: http://ogp.me/ns/object#\">
             <meta property=\"fb:app_id\"      content=\"%s\" />
             <meta property=\"og:type\"        content=\"%s\" />
             <meta property=\"og:url\"         content=\"%s\" />
             <meta property=\"og:title\"       content=\"%s\" />
             <meta property=\"og:description\" content=\"%s\" />
             <meta property=\"og:image\"       content=\"%s\" />
            ",
            $this->application->getId(),
            $this->object_type,
            $this->url,
            $this->title,
            $this->description,
            $this->image_url
        );

        return $head;
    }
}
