<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   facebook-utils
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2013 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://github.com/chrisnoden
 */

namespace ChrisNoden\Facebook\OpenGraph;

use ChrisNoden\Facebook\Exception\FacebookApiException;
use ChrisNoden\Facebook\Exception\FacebookAuthException;
use ChrisNoden\Facebook\Exception\FacebookConnectionException;
use ChrisNoden\Facebook\Exception\FacebookInsufficientPermissions;
use ChrisNoden\Facebook\Graph\AccessToken\AccessTokenAbstract;
use Guzzle\Http\Exception\BadResponseException;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Guzzle\Http\Exception\CurlException;
use Guzzle\Http\Message\Request;
use Guzzle\Http\Message\Response;
use ChrisNoden\Facebook\Graph\Object\Application;

/**
 * Class ObjectAbstract
 * All Open Graph objects are derived from this Abstract Class
 *
 * @category ChrisNoden\Facebook\OpenGraph
 * @package  facebook-utils
 * @author   Chris Noden <chris.noden@gmail.com>
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link     https://github.com/chrisnoden/synergy
 * @link     https://developers.facebook.com/docs/opengraph/using-object-api/
 */
abstract class ObjectAbstract
{
    /** @var string */
    protected $id;
    /** @var Application */
    protected $application;
    /** @var string */
    protected $title;
    /** @var string */
    protected $image_url;
    /** @var string */
    protected $url;
    /** @var string */
    protected $description;
    /** @var array */
    protected $data = array();
    /** @var AccessTokenAbstract */
    protected $access_token;
    /** @var string */
    protected $app_namespace;
    /** @var string */
    protected $object_type;
    /** @var int */
    protected $connection_timeout = 7;


    public function create()
    {
        $request = $this->getGraphCreateRequest();

        $response = $this->makeGraphRequest($request);

        if ($results = json_decode($response, true)) {
            $this->id = $results['id'];
        }
    }


    /**
     * Connect to Facebook Graph with the Request data
     *
     * @param Request $request
     *
     * @return \Guzzle\Http\EntityBodyInterface|string
     * @throws \ChrisNoden\Facebook\Exception\FacebookApiException
     * @throws \ChrisNoden\Facebook\Exception\FacebookAuthException
     * @throws \ChrisNoden\Facebook\Exception\FacebookConnectionException
     * @throws \ChrisNoden\Facebook\Exception\FacebookInsufficientPermissions
     */
    protected function makeGraphRequest(Request $request)
    {
        try {
            $response = $request->send();
            if ($response->getStatusCode() == 200) {
                return ($response->getBody(true));
            }
        } catch (ClientErrorResponseException $ex) {
            $response = $ex->getResponse();
            if ($response instanceof Response) {
                if ($response->getStatusCode() == 403) {
                    // Permissions not granted
                    throw new FacebookInsufficientPermissions(
                        'Insufficient permissions'
                    );
                } elseif ($response->getStatusCode() == 400) {
                    $json = $response->getBody(true);
                    $arr  = json_decode($json, true);
                    if (is_array($arr)) {
                        throw new FacebookAuthException($request->getUrl() . ' : ' . $arr['error']['message']);
                    } else {
                        throw new FacebookAuthException($request->getUrl() . ' : ' . 'Facebook request rejected');
                    }
                } else {
                    throw new FacebookApiException(
                        $response->getMessage()
                    );
                }
            }
        } catch (BadResponseException $ex) {
            throw new FacebookApiException('Facebook Graph Request Failed: ' . $ex->getMessage());
        } catch (CurlException $ex) {
            // connection timed out
            throw new FacebookConnectionException('Unable to connect to Facebook');
        }
    }


    /**
     * @return array
     */
    public function getObject()
    {
        return array(
            'title'       => $this->title,
            'image'       => $this->image_url,
            'url'         => $this->url,
            'description' => $this->description,
            'data'        => $this->data
        );
    }


    /**
     * Set the value of data member
     *
     * @param array $data
     *
     * @return void
     */
    public function setData($data)
    {
        $this->data = $data;
    }


    /**
     * Value of member data
     *
     * @return array value of member
     */
    public function getData()
    {
        return $this->data;
    }


    /**
     * Set the value of description member
     *
     * @param string $description
     *
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }


    /**
     * Value of member description
     *
     * @return string value of member
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * Set the value of image_url member
     *
     * @param string $image_url
     *
     * @return void
     */
    public function setImageUrl($image_url)
    {
        $this->image_url = $image_url;
    }


    /**
     * Value of member image_url
     *
     * @return string value of member
     */
    public function getImageUrl()
    {
        return $this->image_url;
    }


    /**
     * Set the value of title member
     *
     * @param string $title
     *
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }


    /**
     * Value of member title
     *
     * @return string value of member
     */
    public function getTitle()
    {
        return $this->title;
    }


    /**
     * Set the value of url member
     *
     * @param string $url
     *
     * @return void
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }


    /**
     * Value of member url
     *
     * @return string value of member
     */
    public function getUrl()
    {
        return $this->url;
    }


    /**
     * Set the value of app_namespace member
     *
     * @param string $app_namespace
     *
     * @return void
     */
    public function setAppNamespace($app_namespace)
    {
        $this->app_namespace = $app_namespace;
    }


    /**
     * Value of member app_namespace
     *
     * @return string value of member
     */
    public function getAppNamespace()
    {
        return $this->app_namespace;
    }


    /**
     * Set the value of object_type member
     *
     * @param string $object_type
     *
     * @return void
     */
    public function setObjectType($object_type)
    {
        $this->object_type = $object_type;
    }


    /**
     * Value of member object_type
     *
     * @return string value of member
     */
    public function getObjectType()
    {
        return $this->object_type;
    }


    /**
     * Set the value of application member
     *
     * @param \ChrisNoden\Facebook\Graph\Object\Application $application
     *
     * @return void
     */
    public function setApplication($application)
    {
        $this->application = $application;
    }


    /**
     * Value of member application
     *
     * @return \ChrisNoden\Facebook\Graph\Object\Application value of member
     */
    public function getApplication()
    {
        return $this->application;
    }
}
