<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   facebook-utils
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2013 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://github.com/chrisnoden
 */

namespace ChrisNoden\Facebook\App;

use ChrisNoden\Facebook\Exception\FacebookAuthException;
use ChrisNoden\Facebook\Exception\InvalidArgumentException;
use ChrisNoden\Facebook\Graph\Object\Application;

/**
 * Class SignedRequest
 *
 * @category ChrisNoden\Facebook\App
 * @package  facebook-utils
 * @author   Chris Noden <chris.noden@gmail.com>
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link     https://github.com/chrisnoden/synergy
 */
class SignedRequest
{

    /**
     * @var Application
     */
    private $application;
    /**
     * @var array payload data
     */
    private $data = array();
    /**
     * @var array fields that could be in the payload data
     */
    private $data_fields = array(
        'code',
        'algorithm',
        'issued_at',
        'user_id',
        'user',
        'oauth_token',
        'expires',
        'app_data',
        'page'
    );
    /**
     * @var string
     */
    private $signed_request;
    /**
     * @var string algorith used for the signed_request
     */
    private $sr_algorith = 'HMAC-SHA256';


    public function __construct(Application $application)
    {
        $this->setApplication($application);
    }


    /**
     * Parses a signed_request and validates the signature.
     *
     * @param string $signed_request A signed token
     *
     * @return array The payload inside it or null if the sig is wrong
     * @throws InvalidArgumentException
     * @throws FacebookAuthException if the data is fake
     */
    protected function parseSignedRequest($signed_request)
    {
        list($encoded_sig, $payload) = explode('.', $signed_request, 2);

        // decode the data
        $sig  = $this->base64UrlDecode($encoded_sig);
        $data = json_decode($this->base64UrlDecode($payload), true);

        // test the payload
        if (!is_array($data)) {
            throw new InvalidArgumentException('Error decoding signed_request');
        }
        if (strtoupper($data['algorithm']) !== $this->sr_algorith) {
            throw new InvalidArgumentException('signed_request algorith not as expected');
        }

        // check sig
        $expected_sig = $this->getSignature($payload);
        if ($sig !== $expected_sig) {
            throw new FacebookAuthException('signed_request fails test');
        }

        // store the data
        foreach ($data as $key => $val) {
            if (in_array($key, $this->data_fields)) {
                $this->data[$key] = $val;
            }
        }

        return true;
    }


    /**
     * Sign the string
     *
     * @param string $string
     *
     * @return string
     */
    private function getSignature($string)
    {
        return hash_hmac(
            'sha256',
            $string,
            $this->application->getSecret(),
            $raw = true
        );
    }


    /**
     * Create a signed_request for the data properties we have set
     *
     * @return string
     */
    public function createSignedRequest()
    {
        $data = json_encode($this->data);
        $payload = $this->base64UrlEncode($data);
        $signature = $this->base64UrlEncode($this->getSignature($payload));

        return $signature . '.' . $payload;
    }


    /**
     * Base64 encoding that doesn't need to be urlencode()ed.
     * Exactly the same as base64_encode except it uses
     *   - instead of +
     *   _ instead of /
     *   No padded =
     *
     * @param string $input base64UrlEncoded string
     *
     * @return string
     */
    protected function base64UrlDecode($input)
    {
        return base64_decode(strtr($input, '-_', '+/'));
    }


    /**
     * Base64 encoding that doesn't need to be urlencode()ed.
     * Exactly the same as base64_encode except it uses
     *   - instead of +
     *   _ instead of /
     *
     * @param string $input string
     *
     * @return string base64Url encoded string
     */
    protected function base64UrlEncode($input)
    {
        $str = strtr(base64_encode($input), '+/', '-_');
        $str = str_replace('=', '', $str);
        return $str;
    }


    /**
     * Set the value of algorithm member
     *
     * @param string $algorithm
     *
     * @return void
     */
    public function setAlgorithm($algorithm)
    {
        $this->data['algorithm'] = $algorithm;
    }


    /**
     * Value of member algorithm
     *
     * @return string value of member
     */
    public function getAlgorithm()
    {
        return $this->data['algorithm'];
    }


    /**
     * Set the value of app_data member
     *
     * @param string $app_data
     *
     * @return void
     */
    public function setAppData($app_data)
    {
        $this->data['app_data'] = $app_data;
    }


    /**
     * Value of member app_data
     *
     * @return string value of member
     */
    public function getAppData()
    {
        return $this->data['app_data'];
    }


    /**
     * Set the value of code member
     *
     * @param string $code
     *
     * @return void
     */
    public function setCode($code)
    {
        $this->data['code'] = $code;
    }


    /**
     * Value of member code
     *
     * @return string value of member
     */
    public function getCode()
    {
        return $this->data['code'];
    }


    /**
     * Set the value of expires member
     *
     * @param int $expires
     *
     * @return void
     */
    public function setExpires($expires)
    {
        $this->data['expires'] = $expires;
    }


    /**
     * Value of member expires
     *
     * @return int value of member
     */
    public function getExpires()
    {
        return $this->data['expires'];
    }


    /**
     * Set the value of issued_at member
     *
     * @param int $issued_at
     *
     * @return void
     */
    public function setIssuedAt($issued_at)
    {
        $this->data['issued_at'] = $issued_at;
    }


    /**
     * Value of member issued_at
     *
     * @return int value of member
     */
    public function getIssuedAt()
    {
        return $this->data['issued_at'];
    }


    /**
     * Set the value of oauth_token member
     *
     * @param string $oauth_token
     *
     * @return void
     */
    public function setOauthToken($oauth_token)
    {
        $this->data['oauth_token'] = $oauth_token;
    }


    /**
     * Value of member oauth_token
     *
     * @return string value of member
     */
    public function getOauthToken()
    {
        return $this->data['oauth_token'];
    }


    /**
     * Set the value of page member
     *
     * @param array $page
     *
     * @return void
     */
    public function setPage($page)
    {
        $this->data['page'] = $page;
    }


    /**
     * Value of member page
     *
     * @return array value of member
     */
    public function getPage()
    {
        return $this->data['page'];
    }


    /**
     * Set the value of user member
     *
     * @param array $user
     *
     * @return void
     */
    public function setUser($user)
    {
        $this->data['user'] = $user;
    }


    /**
     * Value of member user
     *
     * @return array value of member
     */
    public function getUser()
    {
        return $this->data['user'];
    }


    /**
     * Set the value of user_id member
     *
     * @param int $user_id
     *
     * @return void
     */
    public function setUserId($user_id)
    {
        $this->data['user_id'] = $user_id;
    }


    /**
     * Value of member user_id
     *
     * @return int value of member
     */
    public function getUserId()
    {
        if (isset($this->data['user_id'])) {
            return $this->data['user_id'];
        }

        return null;
    }


    /**
     * Set and parse the signed_request
     *
     * @param string $signed_request
     *
     * @throws \ChrisNoden\Facebook\Exception\InvalidArgumentException
     */
    public function setSignedRequest($signed_request)
    {
        if (is_string($signed_request) && $this->parseSignedRequest($signed_request)) {
            $this->signed_request = $signed_request;
        } else {
            throw new InvalidArgumentException('Invalid signed_request');
        }
    }


    /**
     * Application must have ID and Secret set
     *
     * @param Application $app
     *
     * @return void
     * @throws InvalidArgumentException
     */
    public function setApplication(Application $app)
    {
        if (is_null($app->getSecret())) {
            throw new InvalidArgumentException('Application must have Secret set');
        } elseif (is_null($app->getId())) {
            throw new InvalidArgumentException('Application must have ID set');
        }
        $this->application = $app;
    }


    /**
     * If we have an access_token for this SignedRequest
     *
     * @return null|string
     */
    public function getAccessToken()
    {
        if ($this->application instanceof Application && !is_null($this->application->getAccessToken())) {
            return $this->application->getAccessToken();
        }

        return null;
    }
}
