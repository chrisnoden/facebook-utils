<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   facebook-utils
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2013 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://github.com/chrisnoden
 */

namespace ChrisNoden\Facebook\App;

use Aws\Ec2\Iterator\DescribeInstancesIterator;
use ChrisNoden\Facebook\Core\FacebookDomainMapping;
use ChrisNoden\Facebook\Graph\AccessToken\UserAccessToken;
use ChrisNoden\Facebook\Graph\Object\Application;
use ChrisNoden\Facebook\Exception\InvalidArgumentException;
use ChrisNoden\Facebook\Graph\Object\User;
use Guzzle\Http\Client;

/**
 * Class WebApp
 *
 * @category ChrisNoden\Facebook\App
 * @package  facebook-utils
 * @author   Chris Noden <chris.noden@gmail.com>
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link     https://github.com/chrisnoden/synergy
 */
class WebApp implements AppInterface
{

    /**
     * @var int
     */
    protected $app_id;
    /**
     * @var string
     */
    protected $app_secret;
    /**
     * @var string our facebook permissions scope
     */
    protected $app_scope = 'user_likes';
    /**
     * @var Application
     */
    protected $application;
    /**
     * @var User
     */
    protected $user;
    /**
     * @var string
     */
    protected $user_id;
    /**
     * @var SignedRequest
     */
    protected $signed_request;
    /**
     * @var string
     */
    protected $csrf_token;
    /**
     * @var array
     */
    protected $persistent_data;
    /**
     * @var string
     */
    protected $session_name;
    /**
     * List of query parameters that get automatically dropped when rebuilding
     * the current URL.
     */
    protected $drop_query_params = array(
        'code',
        'state',
        'signed_request',
    );
    /**
     * @var bool do we trust proxy forwarded headers
     */
    protected $trustForwarded = false;
    /**
     * @var int how long to wait for Facebook HTTP connections to timeout
     */
    protected $connection_timeout = 10;
    /**
     * @var UserAccessToken
     */
    protected $access_token;


    public function __construct(Application $application)
    {
        if (!session_id()) {
            @session_start();
        }

        $this->setApplication($application);

        $this->init();
    }


    /**
     * Check, Load and Init the WebApp
     *
     * @throws InvalidArgumentException if any dependancy is not met
     */
    protected function init()
    {
        if (is_null($this->application->getId())) {
            throw new InvalidArgumentException('Application not configured with ID');
        } elseif (is_null($this->application->getSecret())) {
            throw new InvalidArgumentException('Application not configured with Secret');
        }
        $this->loadPersistentData();

        $this->findSignedRequest();

        if ($code = $this->getCode()) {
            $access_token = $this->getAccessTokenFromCode($code);
            if ($access_token) {
                $this->access_token = $access_token;
                $this->setPersistentData('code', $code);
                $this->setPersistentData('access_token', $access_token);
                $this->fetchCurrentFacebookWebUser();
            } else {
                $this->clearAllPersistedData();
            }
        }
    }


    /**
     * Fetch the persistent data array from the session
     */
    protected function loadPersistentData()
    {
        $this->session_name = 'app_' . $this->app_id;

        if (isset($_SESSION[$this->session_name])) {
            $this->persistent_data = $_SESSION[$this->session_name];

            // set the csrf_token property value
            if (isset($this->persistent_data['csrf_token'])) {
                $this->csrf_token = $this->persistent_data['csrf_token'];
            }

            // access_token
            if (isset($this->persistent_data['access_token'])) {
                $this->access_token = $this->persistent_data['access_token'];
            }

            // signed request
            if (isset($this->persistent_data['signed_request']) &&
                $this->persistent_data['signed_request'] instanceof SignedRequest
            ) {
                $this->signed_request = $this->persistent_data['signed_request'];
                if (!isset($this->access_token) && !is_null($this->signed_request->getAccessToken())) {
                    $this->access_token = new UserAccessToken();
                    $this->access_token->setAppId($this->app_id);
                    $this->access_token->setAccessToken($this->signed_request->getAccessToken());
                }
            }
        }
    }


    /**
     * Look for the signed request and assign it to the SignedRequest object to parse
     *
     * @return void
     */
    protected function findSignedRequest()
    {
        if (isset($_COOKIE['fbsr_' . $this->app_id])) {
            $this->signed_request = new SignedRequest($this->application);
            $this->signed_request->setSignedRequest($_COOKIE['fbsr_' . $this->app_id]);
            $this->setPersistentData('signed_request', $this->signed_request);
        } elseif (isset($_REQUEST['signed_request'])) {
            $this->signed_request = new SignedRequest($this->application);
            $this->signed_request->setSignedRequest($_REQUEST['signed_request']);
            $this->setPersistentData('signed_request', $this->signed_request);
        }

        if ($this->signed_request instanceof SignedRequest && !is_null($this->signed_request->getUserId())) {
            $this->setUserId($this->signed_request->getUserId());
        }
    }


    /**
     * @return SignedRequest
     */
    public function getSignedRequest()
    {
        return $this->signed_request;
    }


    /**
     * Value of member application
     *
     * @return Application value of member
     */
    public function getApplication()
    {
        return $this->application;
    }


    /**
     * Set the Application for the Web App
     *
     * @param Application $application
     *
     * @return $this
     */
    public function setApplication(Application $application)
    {
        $this->application = $application;
        $this->app_id      = $application->getId();
        $this->app_secret  = $application->getSecret();

        return $this;
    }


    /**
     * persist the data
     *
     * @param string $property
     * @param mixed  $value
     */
    public function setPersistentData($property, $value)
    {
        $this->persistent_data[$property] = $value;
        $_SESSION[$this->session_name]    = $this->persistent_data;
    }


    /**
     * get the value of the stored data
     *
     * @param string $property
     *
     * @return mixed
     */
    public function getPersistentData($property)
    {
        if (isset($this->persistent_data[$property])) {
            return $this->persistent_data[$property];
        }
        return null;
    }


    /**
     * remove the persisted session data
     *
     * @param string $property
     *
     * @return void
     */
    public function clearPersistedData($property)
    {
        $this->persistent_data[$property] = null;
        unset($this->persistent_data[$property]);
        $_SESSION[$this->session_name] = $this->persistent_data;
    }


    /**
     * remove all the persisted session data
     */
    public function clearAllPersistedData()
    {
        $this->persistent_data         = array();
        $_SESSION[$this->session_name] = $this->persistent_data;
    }


    /**
     * Generate a random string - persist it to the session
     *
     * @return string the token
     */
    protected function generateCsrfToken()
    {
        $token = md5(uniqid(mt_rand(), true));
        $this->setPersistentData('csrf_token', $token);
        $this->csrf_token = $token;

        return $token;
    }


    /**
     * @return string
     */
    public function getCsrfToken()
    {
        // do we need a new CSRF Token
        if (is_null($this->csrf_token)) {
            $this->generateCsrfToken();
        }

        return $this->csrf_token;
    }


    /**
     * Clear the CSRF Token from the persistent session data
     */
    public function clearCsrfToken()
    {
        $this->csrf_token = null;
        $this->clearPersistedData('csrf_token');
    }


    /**
     * Get a Login URL for use with redirects. By default, full page redirect is
     * assumed. If you are using the generated URL with a window.open() call in
     * JavaScript, you can pass in display=popup as part of the $params.
     *
     * The parameters:
     * - redirect_uri: the url to go to after a successful login
     * - scope: comma separated list of requested extended perms
     *
     * @param array $params Provide custom parameters
     *
     * @return string The URL for the login flow
     */
    public function getLoginUrl($params = array())
    {
        return $this->getUrl(
            'www',
            'dialog/oauth',
            array_merge(
                array(
                    'client_id'    => $this->app_id,
                    'redirect_uri' => $this->application->getAppCanvasPageUrl(), // possibly overwritten
                    'state'        => $this->getCsrfToken(),
                    'scope'        => $this->application->getAppScopeString(),
                ),
                $params
            )
        );
    }


    /**
     * Returns the Current URL, stripping it of known FB parameters that should
     * not persist.
     *
     * @return string The current URL
     */
    protected function getCurrentUrl()
    {
        // construct the current page URL
        $protocol = $this->getHttpProtocol();
        $host     = $this->getHttpHost();
        if (isset($_SERVER['REQUEST_URI'])) {
            $request_uri = $_SERVER['REQUEST_URI'];
        } else {
            $request_uri = '';
        }
        if (!$protocol) {
            $currentUrl = $this->application->getAppCanvasPageUrl();
        } else {
            $currentUrl = sprintf(
                '%s://%s%s',
                $protocol,
                $host,
                $request_uri
            );
        }

        // parse out the URL we've constructed
        $parts = parse_url($currentUrl);

        // re-build any querystring for the current URL
        $query = '';
        if (!empty($parts['query'])) {
            // drop known fb params
            $params          = explode('&', $parts['query']);
            $retained_params = array();
            foreach ($params as $param) {
                if ($this->shouldRetainParam($param)) {
                    $retained_params[] = $param;
                }
            }

            if (!empty($retained_params)) {
                $query = '?' . implode($retained_params, '&');
            }
        }

        // use port if non default
        $port =
            isset($parts['port']) &&
            (($protocol === 'http' && $parts['port'] !== 80) ||
                ($protocol === 'https' && $parts['port'] !== 443))
                ? ':' . $parts['port'] : '';

        // rebuild
        $url = sprintf(
            '%s://%s%s%s%s',
            $protocol,
            $parts['host'],
            $port,
            $parts['path'],
            $query
        );
        return $url;
    }


    /**
     * @return string|bool
     */
    protected function getHttpHost()
    {
        if ($this->trustForwarded && isset($_SERVER['HTTP_X_FORWARDED_HOST'])) {
            return $_SERVER['HTTP_X_FORWARDED_HOST'];
        } elseif (isset($_SERVER['HTTP_HOST'])) {
            return $_SERVER['HTTP_HOST'];
        }

        return false;
    }


    /**
     * @return string
     */
    protected function getHttpProtocol()
    {
        if ($this->trustForwarded && isset($_SERVER['HTTP_X_FORWARDED_PROTO'])) {
            if ($_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
                return 'https';
            }
            return 'http';
        }
        /*apache + variants specific way of checking for https*/
        if (isset($_SERVER['HTTPS']) &&
            ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] == 1)
        ) {
            return 'https';
        }
        /*nginx way of checking for https*/
        if (isset($_SERVER['SERVER_PORT']) &&
            ($_SERVER['SERVER_PORT'] === '443')
        ) {
            return 'https';
        }
        return 'http';
    }


    /**
     * Build the URL for given domain alias, path and parameters.
     *
     * @param $name string The name of the domain
     * @param $path string Optional path (without a leading slash)
     * @param $params array Optional query parameters
     *
     * @return string The URL for the given parameters
     */
    protected function getUrl($name, $path = '', $params = array())
    {
        $url = FacebookDomainMapping::memberByKey(strtoupper($name))->value();
        if ($path) {
            if ($path[0] === '/') {
                $path = substr($path, 1);
            }
            $url .= $path;
        }
        if ($params) {
            $url .= '?' . http_build_query($params, null, '&');
        }

        return $url;
    }


    /**
     * Returns true if and only if the key or key/value pair should
     * be retained as part of the query string.  This amounts to
     * a brute-force search of the very small list of Facebook-specific
     * params that should be stripped out.
     *
     * @param string $param A key or key/value pair within a URL's query (e.g.
     *                     'foo=a', 'foo=', or 'foo'.
     *
     * @return boolean
     */
    protected function shouldRetainParam($param)
    {
        foreach ($this->drop_query_params as $drop_query_param) {
            if (strpos($param, $drop_query_param . '=') === 0) {
                return false;
            }
        }

        return true;
    }


    /**
     * Get the authorization code from the query parameters, if it exists,
     * and otherwise return false to signal no authorization code was
     * discoverable.
     *
     * @return mixed The authorization code, or false if the authorization
     *               code could not be determined.
     */
    protected function getCode()
    {
        if (isset($_REQUEST['code'])) {
            if ($this->getCsrfToken() !== null &&
                isset($_REQUEST['state']) &&
                $this->getCsrfToken() === $_REQUEST['state']
            ) {
                // CSRF state has done its job, so clear it
                $this->clearCsrfToken();
                return $_REQUEST['code'];
            }
        }

        return false;
    }


    /**
     * Retrieves an access token for the given authorization code
     * (previously generated from www.facebook.com on behalf of
     * a specific user).  The authorization code is sent to graph.facebook.com
     * and a legitimate access token is generated provided the access token
     * and the user for which it was generated all match, and the user is
     * either logged in to Facebook or has granted an offline access permission.
     *
     * @param string $code An authorization code
     *
     * @return UserAccessToken An access token exchanged for the authorization code, or
     *                         false if an access token could not be generated.
     */
    protected function getAccessTokenFromCode($code)
    {
        if (empty($code)) {
            return false;
        }
        if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ||
            (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) {
            $isSSL = true;
        } else {
            $isSSL = false;
        }

        $redirect_uri = sprintf('http%s://%s%s', $isSSL ? 's' : '', $_SERVER['HTTP_HOST'], $_SERVER['REQUEST_URI']);

        $this->access_token = UserAccessToken::create($this->app_id, $this->app_secret, $code, $redirect_uri);

        return $this->access_token;
    }


    /**
     * Uses the user access_token to get the current Facebook user object from Graph
     *
     * @return bool
     */
    protected function fetchCurrentFacebookWebUser()
    {
        if (isset($this->access_token)) {
            $client = new Client(FacebookDomainMapping::GRAPH()->value());
            $client->setConfig(array('exceptions' => true));
            $request = $client->get(
                sprintf(
                    '/me?access_token=%s',
                    $this->access_token->getAccessToken()
                ),
                array(),
                array(
                    'exceptions'      => true,
                    'connect_timeout' => $this->connection_timeout,
                )
            );

            $this->user = null;

            try {
                $response = $request->send();
                if ($response->getStatusCode() == 200) {
                    $user = new User();
                    $user->setObjectFieldsFromJson($response->getBody(true));
                    $user->setAccessToken($this->access_token);
                    $this->user = $user;
                }
            } catch (\Exception $ex) {
                // most likely that user very recently revoked authorization.
                // In any event, we don't have an access token, so say so.
            }
        }
    }


    /**
     * Value of member user
     *
     * @return User value of member
     */
    public function getUser()
    {
        if (!$this->user instanceof User && isset($this->user_id)) {
            $user = new User();
            if ($this->access_token instanceof UserAccessToken) {
                $user->setAccessToken($this->access_token);
            }
            $user->load($this->user_id);
            $this->user = $user;
        }

        return $this->user;
    }


    /**
     * The Facebook User ID of the current website user
     *
     * @return string a very big number
     */
    public function getUserId()
    {
        return $this->user_id;
    }


    /**
     * Loads the user property with the User object for the given user_id
     *
     * @param string $user_id a big facebook ID
     */
    protected function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }


    /**
     * The logged in user's Access Token
     *
     * @return string
     */
    public function getAccessToken()
    {
        if (isset($this->persistent_data['access_token']) &&
            $this->persistent_data['access_token'] instanceof UserAccessToken
        ) {
            /** @var UserAccessToken $at */
            $at = $this->persistent_data['access_token'];
            return $at->getAccessToken();
        }
        if ($this->access_token instanceof UserAccessToken) {
            return $this->access_token->getAccessToken();
        }
    }
}
