<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Abstract Class
 * @package   facebook-graph
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2013 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://github.com/chrisnoden
 */

namespace ChrisNoden\Facebook\Graph\AccessToken;

use ChrisNoden\Facebook\Exception\InvalidArgumentException;
use ChrisNoden\Facebook\Graph\Api\GraphRequest;
use ChrisNoden\Facebook\Exception\FacebookConnectionException;
use ChrisNoden\Facebook\Graph\Object\Application;

/**
 * Class AccessTokenAbstract
 * Facebook Access Tokens inherit this class
 *
 * @category  Graph\AccessToken
 * @package   facebook-graph
 * @author    Chris Noden <chris.noden@gmail.com>
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://github.com/chrisnoden
 */
abstract class AccessTokenAbstract
{

    /** @var \DateTime when does this token expire */
    protected $expires_at;
    /** @var bool */
    protected $is_valid = true;
    /** @var string the Facebook access_token value */
    protected $access_token;
    /** @var array */
    protected $token_info = array();
    /** @var AccessTokenType */
    protected $access_token_type;
    /** @var string */
    protected $app_id;
    /** @var string */
    protected $app_secret;


    /**
     * Return all the info about the Token that Facebook provides
     *
     * @return array
     * @throws FacebookConnectionException if unable to connect to Facebook over HTTPS
     */
    public function getTokenInfo()
    {
        if (count($this->token_info) == 0) {
            $this->fetchTokenData();
        }

        return $this->token_info;
    }


    protected function fetchTokenData()
    {
        if (!isset($this->access_token)) {
            throw new InvalidArgumentException('Must set or load access_token before '.__METHOD__);
        }

        if ($this->access_token_type == AccessTokenType::USER && isset($this->app_id) && isset($this->app_secret)) {
            $appAccessToken = AppAccessToken::create($this->app_id, $this->app_secret);
        } else {
            $appAccessToken = $this->access_token;
        }

        $graph_request = new GraphRequest();
        $client        = $graph_request->getClient();
        $request       = $client->get(
            sprintf(
                '/debug_token?input_token=%s&access_token=%s',
                $this->access_token,
                $appAccessToken
            ),
            null,
            $graph_request->getClientOptions()
        );
        $response      = $request->send();
        if ($response->getStatusCode() == 200) {
            $json = $response->getBody(true);
            if ($arr = json_decode($json, true)) {
                $this->token_info = $arr['data'];
                if (isset($arr['data']['is_valid'])) {
                    $this->is_valid = $arr['data']['is_valid'];
                }
            }
        } else {
            throw new FacebookConnectionException(
                sprintf('Facebook error: %s', $response->getBody(true))
            );
        }

    }


    /**
     * @return string the access token string
     */
    public function __toString()
    {
        if (!is_null($this->access_token)) {
            return sprintf("%s", $this->access_token);
        }
        return '';
    }


    /**
     * Value of member access_token
     *
     * @return string value of member
     */
    public function getAccessToken()
    {
        return $this->access_token;
    }


    /**
     * Set the access token string if you've obtained it elsewhere
     *
     * @param string $access_token
     */
    public function setAccessToken($access_token)
    {
        $this->access_token = $access_token;
    }


    /**
     * Set the value of app_id member
     *
     * @param string $app_id
     *
     * @return void
     */
    public function setAppId($app_id)
    {
        $this->app_id = $app_id;
    }


    /**
     * Value of member app_id
     *
     * @return string value of member
     */
    public function getAppId()
    {
        return $this->app_id;
    }


    /**
     * Set the value of app_secret member
     *
     * @param string $app_secret
     *
     * @return void
     */
    public function setAppSecret($app_secret)
    {
        $this->app_secret = $app_secret;
    }


    /**
     * Is the token valid (defaults to true)
     *
     * @return bool
     */
    public function isValid()
    {
        return $this->is_valid;
    }
}
