<?php
/**
 * Created by Chris Noden using JetBrains PhpStorm.
 *
 * PHP version 5
 *
 * This code is copyright and is the intellectual property
 * of the copyright holder named below. It may not be copied,
 * re-distributed, modified, reverse engineered or used without
 * the express written permission of the copyright holder.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  File
 * @package   gmb-webv2
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2013 Chris Noden
 * @license   https://www.chrisnoden.com/CLIENT-LICENSE.md Proprietary
 * @link      https://github.com/chrisnoden
 */

namespace ChrisNoden\Facebook\Graph\AccessToken;

use ChrisNoden\Facebook\Core\FacebookDomainMapping;
use ChrisNoden\Facebook\Exception\FacebookConnectionException;

/**
 * Class UserAccessToken
 *
 * @category GmbAdmin\Facebook
 * @package  gmb-webv2
 * @author   Chris Noden <chris.noden@gmail.com>
 * @license  https://www.chrisnoden.com/CLIENT-LICENSE.md Proprietary
 * @link     https://github.com/chrisnoden/synergy
 */
class UserAccessToken extends AccessTokenAbstract
{

    /** @var AccessTokenType */
    protected $access_token_type = AccessTokenType::USER;


    /**
     * @param string $app_id
     * @param string $app_secret   app secret
     * @param string $code         An authorization code
     * @param string $redirect_url should be same as Login URL during oAuth step
     *
     * @return UserAccessToken
     */
    public static function create($app_id, $app_secret, $code, $redirect_url)
    {
        $token = new UserAccessToken();
        $token->setAppId($app_id);
        $token->setAppSecret($app_secret);
        $token->fetchAccessTokenFromCode($app_secret, $code, $redirect_url);
        return $token;
    }


    /**
     * Retrieves an access token for the given authorization code
     * (previously generated from www.facebook.com on behalf of
     * a specific user).  The authorization code is sent to graph.facebook.com
     * and a legitimate access token is generated provided the access token
     * and the user for which it was generated all match, and the user is
     * either logged in to Facebook or has granted an offline access permission.
     *
     * @param string $app_secret   app secret
     * @param string $code         An authorization code
     * @param string $redirect_url should be same as Login URL during oAuth step
     *
     * @return UserAccessToken An access token exchanged for the authorization code, or
     *                         false if an access token could not be generated.
     */
    public function fetchAccessTokenFromCode($app_secret, $code, $redirect_url)
    {
        $url = sprintf(
            '%s/oauth/access_token?client_id=%s&client_secret=%s&redirect_uri=%s&code=%s',
            FacebookDomainMapping::GRAPH,
            $this->app_id,
            $app_secret,
            $redirect_url,
            $code
        );

        $ch = curl_init();
        curl_setopt_array(
            $ch,
            array(
                CURLOPT_CONNECTTIMEOUT => 10,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT        => 60,
                CURLOPT_USERAGENT      => 'chrisnoden_facebook_utils',
                CURLOPT_URL            => $url,
            )
        );
        $access_token_response = curl_exec($ch);

        if ($access_token_response !== false) {
            $response_params = array();
            parse_str($access_token_response, $response_params);
            if (!isset($response_params['access_token'])) {
                return false;
            }

            $this->access_token = $response_params['access_token'];
        } else {
            throw new FacebookConnectionException('Error connecting to Facebook API');
        }

        return $this;
    }
}
